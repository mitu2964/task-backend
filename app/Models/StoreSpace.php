<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreSpace extends Model
{
    use HasFactory;
    protected $table = 'store_spaces';
    protected $primaryKey = 'id';
    protected $fillable = ['title','description','image','created_at','updated_at','deleted_at'];
}
