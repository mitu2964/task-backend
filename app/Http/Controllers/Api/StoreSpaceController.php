<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\StoreSpace;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Http\Resources\StoreSpecResource;


class StoreSpaceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $storeSpaces = StoreSpace::all();
        $response = [
            'success' => true,
            'data'    => StoreSpecResource::collection($storeSpaces)
        ];  
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->except('image');
        if ($request->hasFile('file')) {
            $img = $request->file('file');
            $fileName = time(). '.' . $img->getClientOriginalExtension();
            $path = 'assets/images/';
            if (!File::exists($path)) File::makeDirectory($path, 0777, true);
            $location = ($path . $fileName);
            Image::make($img)->resize(200, 200)->save($location);
            $data['image'] = $location;
            StoreSpace::create($data);
            $response = [
                'success' => true,
                'data'    => "Data saved successfully."
            ];    
            return response()->json($response, 200);
        } 
        $response = [
            'success' => false,
            'data'    => "Something went wrong!"
        ]; 
        return response()->json($response, 405);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy()
    {
        // StoreSpace::truncate();
    }
}
